use std::io;
use crossterm::event::{Event, KeyCode, read};
use crossterm::event::{DisableMouseCapture, EnableMouseCapture};
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen
};
use reqwest::Client;
use reqwest::Method;
use tui::backend::CrosstermBackend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Modifier, Style};
use tui::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, List, ListItem, Paragraph, Wrap};
use tui_textarea::TextArea;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::new();
    let res = client.request(Method::OPTIONS, "https://i.instagram.com/api/v1/direct_v2/inbox/?persistentBadging=true&folder=&limit=3&thread_message_limit=15")
        .header("x-ig-app-id", "")
        .header("x-csrftoken", "")
        .header("cookie", "")
        .send().await?.json::<serde_json::Value>().await?;
    let inbox = res.get("inbox").unwrap();
    let threads = inbox.get("threads").unwrap().as_array().unwrap();

    let messages = threads[2].get("items").unwrap().as_array().unwrap();

    let mut stack = vec!();
    messages.iter().for_each(|message| {
        if let Some(content) = message.get("text") {
            let user = if message.get("user_id").unwrap().as_u64().unwrap() == 39850026868 { "claire.dnx" } else {"simon.scatton"};
            let res = format!("<{}> {}", user, &content.as_str().unwrap()[0..content.as_str().unwrap().len()]);
            stack.insert(0, res);
        }
    });


    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    enable_raw_mode()?;
    crossterm::execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let mut textarea = TextArea::default();
    textarea.set_block(
        Block::default()
            .title("Message")
            .borders(Borders::ALL)
    );

    loop {
        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints(
                    [
                        Constraint::Percentage(80),
                        Constraint::Percentage(20),
                    ].as_ref()
                )
                .split(f.size());

            let messages = stack.iter().map(|m| Spans::from(vec!(Span::styled(m, Style::default())))).collect::<Vec<_>>();

            let messages_panel = Paragraph::new(messages)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .title(Span::styled("Claire", Style::default().add_modifier(Modifier::BOLD))),
                )
                .alignment(Alignment::Left)
                .wrap(Wrap { trim: false });

            f.render_widget(messages_panel, chunks[0]);

            f.render_widget(textarea.widget(), chunks[1]);
        })?;

        if let Ok(Event::Key(key)) = read() {
            if key.code == KeyCode::Esc {
                break;
            }
            textarea.input(key);
        }
    }

    disable_raw_mode()?;
    crossterm::execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    Ok(())
}